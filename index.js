const express = require("express");
const path = require("path");

var app = express();
var router = express.Router();

app.use("/api", router);

router.use((request, response, next) => {
    next();
})

app.get("/api", (req, res) => res.status(200).send("Croco's server"));
app.use("/api", express.static(path.resolve() + "\\resource"));

router.route("/croco").get((request, response) => {
    response.sendFile(path.join(path.resolve(), "/resource/index.html"));
})
var port = 8080;
app.listen(port);
console.log("API listen on port " + port);