# Everything you need to know

Getting started with Git Bash

## First Time Git Setup
Saving your credentials

>**$ git config --global user.name "John Doe"**

>**$ git config --global user.email jogn@email.com**

>>If you got the wrong credentials just go to **Search** -> **Credential Manager** -> **Windows Credentials** and search for git:https://gitlab.com and press **Remove** or **Edit** 


## New Repository

Add an existing folder to a repository
>**$ git init**

>**$ git remote add origin [https link from GitLab repository]**

>**$ git add .**

>**$ git commit -m [your commit message]**

>**$ git push -u origin master**

## Clone existing repository

If you have an existing repository on GitLab just clone the HTTPS link and then:
>**$ git clone [https link from GitLab repository]**
>

## Switch branch

If it's an existing branch:
>**$ git checkout [branch name]**
>
If you want a new branch:
>**$ git checkout -b [branch name]**
>>Instead of $ git push use **$ git push --set-upstream origin [branch name]**

## Add your files to a GitLab repository

Add your files in the repository folder and then:
>**$ git add .**

>**$ git commit -m [your commit message]**

>**$ git push**
>>If you want to see what files changed **$ git status**

>>If you want to add just one file use **$ git add [file name]**
## Ignore files
Create a new file with .gitignore extension and write inside the path from gitignore file to the directory you want to ignore.
>For example: /node_modules
### Hope it was helpful!